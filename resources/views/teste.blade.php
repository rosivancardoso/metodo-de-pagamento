<?php
    require base_path('vendor/autoload.php');
    //require __DIR__.'/../vendor/autoload.php';
    MercadoPago\SDK::setAccessToken(config('services.mercadopago.token'));

    // Cria um objeto de preferência
    $preference = new MercadoPago\Preference();

    // Cria um item na preferência
    $item = new MercadoPago\Item();
    $item->title = 'Meu produto';
    $item->quantity = 1;
    $item->unit_price = 75.56;
    $preference->items = array($item);
    $preference->save();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TESTE DE PAGAMENTO</title>
</head>
<body>
    <div class="">
        <h1>Hello laravel</h1>
    </div>
    
</body>
// SDK MercadoPago.js V2
<script src="https://sdk.mercadopago.com/js/v2"></script>

<script>
// Adicione as credenciais do SDK
  const mp = new MercadoPago("{{config('services.mercadopago.key')}}", {
        locale: 'es-AR'
  });

  // Inicialize o checkout
  mp.checkout({
      preference: {
          id: ' {{ $preference->id }}'
      },
      render: {
            container: '.cho-conta iner', // Indique o nome da class onde será exibido o botão de pagamento
            label: 'Pagar', // Muda o texto do botão de pagamento (opcional)
      }
});
</script>

</html>
